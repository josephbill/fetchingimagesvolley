package com.first.fetchingimages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.first.fetchingimages.adapter.ViewPagerAdapter;
import com.first.fetchingimages.model.SliderUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    private ImageView[] dots;

    RequestQueue rq;
    List<SliderUtils> sliderImg;
    ViewPagerAdapter viewPagerAdapter;

    String request_url = "https://www.global.mcarfix.com/api/view/adverts";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rq = CustomVolleyRequest.getInstance(this).getRequestQueue();
        sliderImg = new ArrayList<>();
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        sendRequest();
    }

    public void sendRequest() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, request_url, null, new Response.Listener<JSONArray>() {
        @Override
        public void onResponse (JSONArray response){
            for (int i = 0; i < response.length(); i++) {

                SliderUtils sliderUtils = new SliderUtils();

                try {
                    JSONObject jsonObject = response.getJSONObject(i);

                    sliderUtils.setSliderImageUrl(jsonObject.getString("image_url"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                sliderImg.add(sliderUtils);

            }

            viewPagerAdapter = new ViewPagerAdapter(sliderImg, MainActivity.this);

            viewPager.setAdapter(viewPagerAdapter);

        }
    },
        new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        CustomVolleyRequest.getInstance(this).addToRequestQueue(jsonArrayRequest);

    }


}
