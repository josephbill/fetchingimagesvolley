package com.first.fetchingimages.model;

public class SliderUtils {
    String sliderImageUrl;

    public String getSliderImageUrl(){
        return sliderImageUrl;
    }

    public void setSliderImageUrl(String sliderImageUrl){
        this.sliderImageUrl = sliderImageUrl;
    }
}
